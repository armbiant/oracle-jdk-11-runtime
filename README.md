# OpenJ9-jdk11 #

This project produces a docker image capable of executing java applications built with jdk11 in the OpenJ9 JVM. It is 
intended to run applications built using Spring Boot, but it works with any jar file capable of executing on its own.
Because it is geared towards Spring Boot applications, additional changes may be required for it to work correctly with
jars produced by other frameworks.

## Getting Started ##

To get started, simply base your image on `registry.gitlab.com/byuhbll/apps/openj9-jdk11`. Then, copy the jar file you
want to run to `/srv/app.jar`.

### Configuration ###

Want to change the configuration on your application? We configure spring to look in `/srv/config` for configuration 
files. Mount your configuration here and set the `spring.profiles.active` environment variable, or do all your 
configuration via environment variables.

### Entrypoint ###

We provide an entrypoint script with this image. It sets some flags on the JVM to optimize ram usage and passes any
docker args to the java application.

### Port Mappings ###

We expose port 8080 for regular application traffic and port 8081 for management traffic.

