package edu.byu.hbll.sandbox;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Api */
@RestController
@RequestMapping("/")
public class Api {

  @Value("${config.value}")
  String value;

  @GetMapping
  public ResponseEntity<?> doHello() {
    return ResponseEntity.ok(value);
  }
}
