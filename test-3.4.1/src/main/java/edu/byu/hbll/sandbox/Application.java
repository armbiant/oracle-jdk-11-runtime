package edu.byu.hbll.sandbox;

import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Defines the entry point for the Spring Boot application and provides application context, such as
 * configuration, which can be injected into other Spring-managed classes.
 */
@SpringBootApplication
@EnableScheduling
@EnableSwagger2
@EnableConfigurationProperties
@ConfigurationProperties
public class Application {

  private static final UncheckedObjectMapper objectMapper = ObjectMapperFactory.newUnchecked();

  /**
   * Launches this application.
   *
   * @param args the command line arguments provided at runtime
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  /**
   * Defines the Jackson ObjectMapper which should be used by Spring for JSON serialization and
   * deserialization (though it can also be used by other classes in lieu of recreating additional
   * instances).
   *
   * @return the object mapper
   */
  @Bean
  public static UncheckedObjectMapper objectMapper() {
    return objectMapper;
  }
}
